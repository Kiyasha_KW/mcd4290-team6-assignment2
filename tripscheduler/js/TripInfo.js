/** 
 * TripInfo.js 
 * This file contains code that runs on load for TripInfo.html
 */
 
"use strict";

function cancelTrip()
{
	
	if (confirm("Cancel this trip?"))
	{
		alert("Trip has been cancelled");
	}
	
	document.location = 'viewscheduled.html';

}