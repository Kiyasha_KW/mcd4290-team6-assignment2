/** 
 * TripScheduling.js 
 * This file contains code that runs on load for TripScheduling.html
 */
 
"use strict";

function scheduleTrip()
{
	
	document.location = 'airportselection.html';
}

function cancelTrip()
{
	document.location = 'index.html';                 // takes user back to home menu
}