/** 
 * RouteSelection.js 
 * This file contains code that runs on load for TripScheduling.html
 */
 
"use strict";

function selectRoute()
{
	document.location = 'routesummary.html';
}
